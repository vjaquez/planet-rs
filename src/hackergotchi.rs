use crate::planet::Subscriber;
use chrono::{DateTime, FixedOffset, Utc};
use reqwest::Url;
use std::collections::HashMap;
use std::sync::Arc;
use tera::{from_value, to_value, try_get_value, Function as TeraFn, Result, Value};

macro_rules! required_arg {
    ($ty: ty, $e: expr, $err: expr) => {
        match $e {
            Some(v) => match from_value::<$ty>(v.clone()) {
                Ok(u) => u,
                Err(_) => return Err($err.into()),
            },
            None => return Err($err.into()),
        }
    };
}

pub struct GetHackergotchi {
    subscribers: Arc<Vec<Subscriber>>,
}

impl GetHackergotchi {
    pub fn new(subscribers: Arc<Vec<Subscriber>>) -> Self {
        GetHackergotchi { subscribers }
    }

    pub fn find_subscriber(&self, url: &Url) -> Option<&Subscriber> {
        for subscriber in &*self.subscribers {
            if url == subscriber.url_ref() {
                return Some(&subscriber);
            }
        }
        None
    }
}

impl TeraFn for GetHackergotchi {
    fn call(&self, args: &HashMap<String, Value>) -> Result<Value> {
        let url = required_arg!(
            String,
            args.get("url"),
            "`get_hackergotchi` requireds atom's `url` argument with a string value"
        );

        if let Ok(parsed_url) = Url::parse(&url) {
            match self.find_subscriber(&parsed_url) {
                Some(subscriber) => return Ok(to_value(subscriber).unwrap()),
                None => return Ok(Value::Null),
            }
        }

        Err(format!("`get_hackergotchi`: couldn't find `{}` in subscribers", url).into())
    }

    fn is_safe(&self) -> bool {
        true
    }
}

pub fn date_human(value: &Value, _: &HashMap<String, Value>) -> Result<Value> {
    let s = try_get_value!("date_human", "value", String, value);
    match s.parse::<DateTime<FixedOffset>>() {
        Ok(val) => {
            let date = val.with_timezone(&Utc);
            let ht = chrono_humanize::HumanTime::from(date);
            Ok(to_value(ht.to_string()).unwrap())
        }
        Err(_) => Ok(to_value(s).unwrap()),
    }
}
