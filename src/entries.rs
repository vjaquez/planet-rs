use crate::planet::Subscriber;

use ammonia;
use atom_syndication::*;
use chrono::{DateTime, FixedOffset};
use feed_rs::{model, parser};
use log::*;
use reqwest::Url;

pub struct Entries {
    pub entries: Vec<Entry>,
}

fn to_text(feed_text: model::Text) -> Text {
    let media_type = match feed_text.content_type.as_str() {
        "text/html" => TextType::Html,
        "application/xhtml+xml" => TextType::Xhtml,
        _ => TextType::Text,
    };

    let content = if media_type == TextType::Html {
        ammonia::clean(&feed_text.content)
    } else {
        feed_text.content
    };

    TextBuilder::default()
        .base(feed_text.src)
        .value(content)
        .r#type(media_type)
        .build()
}

fn to_person(feed_person: model::Person) -> Person {
    PersonBuilder::default()
        .email(feed_person.email)
        .name(feed_person.name)
        .uri(feed_person.uri)
        .build()
}

fn to_link(url: &Url, feed_link: model::Link) -> Link {
    let mut builder = LinkBuilder::default();

    builder
        .href(url.join(feed_link.href.as_str()).unwrap())
        .hreflang(feed_link.href_lang)
        .mime_type(feed_link.media_type)
        .title(feed_link.title);

    if let Some(rel) = feed_link.rel {
        builder.rel(rel);
    }
    if let Some(length) = feed_link.length {
        builder.length(Some(length.to_string()));
    }

    builder.build()
}

impl Entries {
    pub fn new() -> Self {
        Entries {
            entries: Vec::new(),
        }
    }

    fn get_source(&self, subscriber: &Subscriber, feed: &model::Feed) -> Source {
        let mut source_builder = SourceBuilder::default();

        for feed_author in &feed.authors {
            source_builder.author(to_person(feed_author.clone()));
        }
        // HACK: add subscriber's author if none in source
        if feed.authors.len() == 0 {
            debug!(
                "Adding subscriber's author as missing one in source {}",
                subscriber.url_ref()
            );
            source_builder.author(
                PersonBuilder::default()
                    .name(subscriber.name().clone().unwrap())
                    .build(),
            );
        }

        if let Some(generator) = &feed.generator {
            source_builder.generator(
                GeneratorBuilder::default()
                    .uri(generator.uri.clone())
                    .version(generator.version.clone())
                    .value(generator.content.clone())
                    .build(),
            );
        }

        if let Some(icon) = &feed.icon {
            source_builder.icon(icon.uri.clone());
        }

        if let Some(logo) = &feed.logo {
            source_builder.logo(logo.uri.clone());
        }

        let mut found = false;
        for feed_link in &feed.links {
            found = feed_link.rel.clone().map_or(false, |rel| rel == "self")
                && feed_link.href == subscriber.url_ref().to_string();
            source_builder.link(to_link(subscriber.url_ref(), feed_link.clone()));
        }
        // HACK: add subscriber's link `self` if it's not in source
        if !found {
            debug!(
                "Adding subscriber's link as missing `self` in source {}",
                subscriber.url_ref()
            );
            let mime = match feed.feed_type {
                model::FeedType::Atom => "application/atom+xml",
                model::FeedType::JSON => "application/json",
                _ =>  "application/rss+xml",
            };
            source_builder.link(
                LinkBuilder::default()
                    .href(subscriber.url())
                    .rel("self")
                    .mime_type(Some(mime.to_string()))
                    .build(),
            );
        }

        if let Some(updated) = &feed.updated {
            source_builder.updated(updated.clone());
        } else if let Some(published) = &feed.published {
            source_builder.updated(published.clone());
        }

        if let Some(title) = &feed.title {
            source_builder.title(to_text(title.clone()));
        }

        // ignoring:
        //   categories, contributors, rights, subtitle

        source_builder.id(feed.id.clone()).build()
    }

    fn get_entry(&self, url: &Url, source: Source, feed_entry: model::Entry) -> Entry {
        let mut entry_builder = EntryBuilder::default();

        if let Some(title) = feed_entry.title {
            entry_builder.title(to_text(title));
        }

        // published definition is nuts
        if let Some(updated) = feed_entry.updated {
            entry_builder.updated(updated);
        }

        if let Some(published) = feed_entry.published {
            let p: DateTime<FixedOffset> = published.into();
            entry_builder.published(Some(p));

            // always fill updated because it will be the sorting key
            if feed_entry.updated.is_none() {
                entry_builder.updated(published);
            }
        }

        for feed_author in feed_entry.authors {
            entry_builder.author(to_person(feed_author));
        }

        for feed_link in feed_entry.links {
            entry_builder.link(to_link(url, feed_link));
        }

        if let Some(summary) = feed_entry.summary {
            entry_builder.summary(to_text(summary));
        }

        if let Some(content) = feed_entry.content {
            let mut content_builder = ContentBuilder::default();

            let media_type = content.content_type.essence().to_string();

            if let Some(content_body) = content.body {
                let body = if media_type == "text/html" {
                    ammonia::Builder::new()
                        .url_relative(ammonia::UrlRelative::RewriteWithBase(url.clone()))
                        .clean(&content_body)
                        .to_string()
                } else {
                    content_body
                };
                content_builder.value(body);
            }

            if let Some(language) = feed_entry.language {
                content_builder.lang(Some(language));
            }

            content_builder.content_type(Some(media_type));

            if let Some(src) = content.src {
                content_builder.base(Some(src.href));
            }

            entry_builder.content(content_builder.build());
        }

        // ignoring:
        //   categories, contributors, rights, extensions
        entry_builder.id(feed_entry.id).source(Some(source)).build()
    }

    fn add_feed(&mut self, subscriber: &Subscriber, feed: model::Feed) {
        let source = self.get_source(subscriber, &feed);

        for link in &source.links {
            if link.rel == "alternate" {
                *subscriber.web_url.lock().unwrap() = Url::parse(&link.href).ok();
                break;
            }
        }

        for feed_entry in feed.entries {
            let entry = self.get_entry(subscriber.url_ref(), source.clone(), feed_entry);
            self.entries.push(entry);
        }
    }

    pub fn add_xml(&mut self, subscriber: &Subscriber, text: String) {
        match parser::parse(text.as_bytes()) {
            Ok(feed) => {
                self.add_feed(subscriber, feed);
            }
            Err(err) => {
                warn!(
                    "Could not parse feed from {:?}: {:?}",
                    subscriber.url_ref(),
                    err
                );
                debug!("Text: {:?}", text)
            }
        }
    }

    pub fn sort_and_truncate(&mut self, items_per_page: usize) {
        self.entries.sort_by(|a, b| {
            let a_dt = a.updated;
            let b_dt = b.updated;
            b_dt.cmp(&a_dt)
        });

        if items_per_page > 0 {
            self.entries.truncate(items_per_page);
        }
    }
}
