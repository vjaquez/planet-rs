use atom_syndication::*;
use ini::{Ini, Properties};
use log::*;
use reqwest::Url;
use serde::Serialize;
use std::env::current_dir;
use std::fs;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

#[derive(Serialize)]
struct Face {
    name: Option<String>,
    face: Option<PathBuf>,
    width: Option<u32>,
    height: Option<u32>,
}

#[derive(Serialize)]
pub struct Subscriber {
    feed_url: Url,
    pub web_url: Mutex<Option<Url>>,
    face: Face,
}

#[derive(Clone)]
pub struct Config {
    feed_timeout: u64,
    log_level: String,
    cache_directory: Option<PathBuf>,
    output_directory: Option<PathBuf>,
    template_files: Vec<PathBuf>,
}

pub struct Planet {
    config: Config,
    feed: Feed,
    items_per_page: u32,
    subscribers: Arc<Vec<Subscriber>>,
}

fn check_or_create_directory(s: &str) -> Option<PathBuf> {
    let path = PathBuf::from(s);
    if !path.exists() {
        match fs::create_dir_all(path.clone()) {
            Ok(()) => Some(path),
            Err(err) => {
                warn!("Could not create directory {}: {:?}", s, err);
                None
            }
        }
    } else {
        Some(path)
    }
}

impl Config {
    fn new(props: &Properties) -> Self {
        return Config {
            feed_timeout: props
                .get("feed_timeout")
                .and_then(|s| s.parse::<u64>().ok())
                .unwrap_or(20),
            log_level: props.get("log_level").unwrap_or("ERROR").to_string(),
            cache_directory: props
                .get("cache_directory")
                .and_then(check_or_create_directory),
            output_directory: props
                .get("output_dir")
                .and_then(check_or_create_directory)
                .or(current_dir().ok()),
            template_files: props
                .get("template_files")
                .map(|s| {
                    let mut templates: Vec<PathBuf> = Vec::new();
                    let tmplts = s.split_whitespace();
                    for tmpl in tmplts {
                        let f = PathBuf::from(tmpl);
                        if f.extension().map_or_else(|| false, |ext| ext == "tmpl") && f.exists() {
                            templates.push(f);
                        }
                    }
                    templates
                })
                .unwrap_or(Vec::<PathBuf>::new()),
        };
    }

    fn default() -> Self {
        Config {
            feed_timeout: 20,
            log_level: "ERROR".to_string(),
            cache_directory: None,
            output_directory: current_dir().ok(),
            template_files: Vec::<PathBuf>::new(),
        }
    }

    pub fn feed_timeout(&self) -> u64 {
        self.feed_timeout
    }

    pub fn cache_directory(&self) -> &Option<PathBuf> {
        &self.cache_directory
    }

    pub fn output_directory(&self) -> &Option<PathBuf> {
        &self.output_directory
    }

    pub fn template_files(&self) -> &Vec<PathBuf> {
        &self.template_files
    }

    pub fn log_level(&self) -> stderrlog::LogLevelNum {
        match self.log_level.as_str() {
            "ERROR" => stderrlog::LogLevelNum::Error,
            "CRITICAL" => stderrlog::LogLevelNum::Warn,
            "WARNING" => stderrlog::LogLevelNum::Info,
            "INFO" => stderrlog::LogLevelNum::Debug,
            "DEBUG" => stderrlog::LogLevelNum::Trace,
            _ => stderrlog::LogLevelNum::Off,
        }
    }
}

impl Face {
    fn new(props: &Properties, default: &Option<Face>) -> Self {
        Face {
            name: props
                .get("name")
                .map(str::to_string)
                .or(default.as_ref().and_then(|d| d.name.clone())),
            face: props
                .get("face")
                .map(PathBuf::from)
                .or(default.as_ref().and_then(|d| d.face.clone())),
            height: props
                .get("faceheight")
                .and_then(|s| s.to_string().parse::<u32>().ok())
                .or(default.as_ref().and_then(|d| d.height.clone())),
            width: props
                .get("facewidth")
                .and_then(|s| s.to_string().parse::<u32>().ok())
                .or(default.as_ref().and_then(|d| d.width.clone())),
        }
    }

    fn default(config: &Ini) -> Option<Self> {
        for (section, props) in config.iter() {
            if let Some(section_name) = section {
                if section_name.to_lowercase() == "default" {
                    return Some(Face {
                        name: props.get("name").map(str::to_string),
                        face: props.get("face").map(PathBuf::from),
                        height: props
                            .get("faceheight")
                            .and_then(|s| s.to_string().parse::<u32>().ok()),
                        width: props
                            .get("facewidth")
                            .and_then(|s| s.to_string().parse::<u32>().ok()),
                    });
                }
            }
        }
        None
    }
}

impl Subscriber {
    fn new(url: Url, props: &Properties, default_face: &Option<Face>) -> Self {
        Self {
            feed_url: url,
            web_url: Mutex::new(None),
            face: Face::new(props, default_face),
        }
    }

    pub fn url(&self) -> Url {
        self.feed_url.clone()
    }

    pub fn url_ref(&self) -> &Url {
        &self.feed_url
    }

    pub fn name(&self) -> &Option<String> {
        &self.face.name
    }
}

impl Planet {
    pub fn new(config: &Ini) -> Self {
        let mut feed = FeedBuilder::default();
        let mut subscribers: Arc<Vec<Subscriber>> = Arc::new(Vec::new());
        let mut cfg = Config::default();
        let default_face = Face::default(config);

        for (section, props) in config.iter() {
            if let Some(section_name) = section {
                if section_name.to_lowercase() == "planet" {
                    // feed builder
                    let title = props.get("name");

                    feed.title(title.unwrap_or("planet-rs"))
                        .generator(
                            GeneratorBuilder::default()
                                .value(env!("CARGO_PKG_NAME"))
                                .uri(option_env!("CARGO_PKG_REPOSITORY").map(str::to_string))
                                .version(option_env!("CARGO_PKG_VERSION").map(str::to_string))
                                .build(),
                        )
                        .lang(props.get("lang").map(str::to_string))
                        .logo(props.get("logo").map(str::to_string))
                        .icon(props.get("icon").map(str::to_string));

                    if let Some(name) = props.get("owner_name") {
                        feed.author(
                            PersonBuilder::default()
                                .name(name)
                                .email(props.get("owner_email").map(str::to_string))
                                .build(),
                        );
                    }

                    if let Some(link) = props.get("link") {
                        feed.link(
                            LinkBuilder::default()
                                .href(link)
                                .title(title.map(str::to_string))
                                .build(),
                        );
                    }

                    // fill later
                    //   updated

                    // other configurations
                    cfg = Config::new(props);
                } else {
                    // subscribers list
                    match Url::parse(section_name) {
                        Ok(url) => {
                            let subscriber = Subscriber::new(url, &props, &default_face);
                            Arc::get_mut(&mut subscribers).unwrap().push(subscriber);
                        }
                        Err(err) => {
                            warn!("Could not parse {:?} as URL: {:?}", section_name, err);
                        }
                    }
                }
                // TODO: template configurations?
            }
        }

        Planet {
            config: cfg,
            feed: feed.build(),
            items_per_page: config
                .get_from(Some("planet"), "items_per_page")
                .and_then(|s| s.parse::<u32>().ok())
                .unwrap_or(0),
            subscribers,
        }
    }

    pub fn set_updated<V>(&mut self, updated: V)
    where
        V: Into<FixedDateTime>,
    {
        self.feed.set_updated(updated);
    }

    pub fn set_entries(&mut self, entries: Vec<Entry>) {
        self.feed.set_entries(entries)
    }

    pub fn items_per_page(&self) -> usize {
        self.items_per_page as usize
    }

    pub fn feed(&self) -> &Feed {
        &self.feed
    }

    pub fn subscribers(&self) -> Arc<Vec<Subscriber>> {
        self.subscribers.clone()
    }

    pub fn config(&self) -> Config {
        self.config.clone()
    }
}
