// Copyright (C) 2024 Victor Jaquez <vjaquez@igalia.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use clap::Parser;
use http_cache_reqwest::{CACacheManager, Cache, CacheMode, HttpCache, HttpCacheOptions};
use ini::Ini;
use log::*;
use std::fs;
use std::path::{Path, PathBuf};
use std::time::Duration;
use tera::Tera;

mod entries;
mod planet;
mod hackergotchi;

use crate::entries::Entries;
use crate::planet::Planet;
use crate::hackergotchi::{date_human, GetHackergotchi};

// TODO:
// use https://lib.rs/crates/chrono-humanize
// group entries by date


#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long, value_name = "FILE")]
    config: Option<PathBuf>,

    #[arg(short, long, action = clap::ArgAction::Count)]
    debug: u8,

    /// Uses any response in the HTTP cache matching the request,
    /// not paying attention to staleness. If there was no response,
    /// it creates a normal request and updates the HTTP cache with the response.
    #[arg(short, long)]
    force_cache: bool,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    let conffile = args
        .config
        .as_deref()
        .unwrap_or(Path::new("/etc/planet-rs.ini"));
    let conf = Ini::load_from_file(conffile)?;
    let mut planet = Planet::new(&conf);
    let config = planet.config();

    // debug level by config file or command args
    let verbosity = match args.debug {
        0 => config.log_level(),
        1 => stderrlog::LogLevelNum::Error,
        2 => stderrlog::LogLevelNum::Warn,
        3 => stderrlog::LogLevelNum::Info,
        4 => stderrlog::LogLevelNum::Debug,
        _ => stderrlog::LogLevelNum::Trace,
    };

    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity)
        .init()?;

    let mut tera = Tera::default();
    for template in config.template_files() {
        debug!("adding template {:?}", template);
        tera.add_template_file(template.clone(), None)?;
    }

    static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);
    let client = reqwest_middleware::ClientBuilder::new(
        reqwest::Client::builder()
            .user_agent(APP_USER_AGENT)
            .timeout(Duration::from_secs(config.feed_timeout()))
            .gzip(true)
            .build()?,
    )
    .with(Cache(HttpCache {
        mode: if args.force_cache {
            CacheMode::ForceCache
        } else {
            CacheMode::Default
        },
        manager: config
            .cache_directory()
            .as_ref()
            .map(|path| CACacheManager {
                path: path.to_path_buf(),
            })
            .unwrap_or_default(),
        options: HttpCacheOptions::default(),
    }))
    .build();

    let mut entries = Entries::new();
    for subscriber in &*planet.subscribers() {
        info!("Fetching {:?}", subscriber.url_ref());
        match client.get(subscriber.url()).send().await {
            Ok(response) => {
                if response.status().is_success() {
                    match response.text().await {
                        Ok(text) => entries.add_xml(&subscriber, text),
                        Err(err) => warn!("Could not decode {:?}: {:?}", subscriber.url_ref(), err),
                    }
                } else {
                    warn!(
                        "HTTP error for {:?}: {:?}",
                        subscriber.url_ref(),
                        response.status()
                    )
                }
            }
            Err(err) => warn!("Could not fetch {:?}: {:?}", subscriber.url_ref(), err),
        };
    }

    entries.sort_and_truncate(planet.items_per_page());
    planet.set_entries(entries.entries);
    // FIXME: use the last entry's date
    planet.set_updated(chrono::Utc::now());

    tera.register_function("get_hackergotchi", GetHackergotchi::new(planet.subscribers()));
    tera.register_filter("date_human", date_human);

    let mut context = tera::Context::new();
    context.insert("feed", planet.feed());
    context.insert("subscribers", &*planet.subscribers());
    context.insert("last_generated", &chrono::Utc::now());

    for template in config.template_files() {
        let mut fname = config
            .output_directory()
            .clone()
            .unwrap_or(PathBuf::from("./".to_string()));
        if let Some(tmpl_name) = template.file_stem() {
            context.insert("filename", &tmpl_name.to_str().unwrap().to_string());
            fname.push(tmpl_name);
            debug!("rendering {:?} to {:?}", template, fname);
            let out = fs::File::create(&fname)?;
            tera.render_to(template.to_str().unwrap(), &context, out)?;
        }
    }

    let mut fname = config
        .output_directory()
        .clone()
        .unwrap_or(PathBuf::from("./".to_string()));
    fname.push("atom.xml");
    let file = fs::File::create(fname)?;
    planet.feed().write_to(file)?;

    Ok(())
}
