# Planet-RS

## A quick replacement for PlanetPlanet/Venus in Rust

And that's it.

The configuration is the same `ini` file as PlanetPlanet and
[Venus](http://intertwingly.net/code/venus/), but the template system is not,
since Planet-RS uses [Tera](https://keats.github.io/tera/docs/), a modern
template engine. It also caches the HTTP requests.

How to run it:

```shell
cargo run -- -ddd -c ./config.ini
```
